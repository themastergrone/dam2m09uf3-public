package wsclient;

import cat.proven.ws.Prova;
import cat.proven.ws.ProvaService;
import cat.proven.ws.User;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import javax.xml.ws.WebServiceRef;
import java.net.URL;



/**
 *
 * @author alumne
 */
public class WsClient {

    private static ProvaService service;
    
    public static void main(String[] args) {
        //Change provaService URL if web service is not on "http://localhost:8080/helloservice/hello?wsdl"    
        
        try{
              
        URL url = new URL("http://localhost:8080/backend/provaService?wsdl");
        
        service = new ProvaService(url);
        
        
            Prova port = service.getProvaPort();
            int menuOption = -1;
            do{
                System.out.println("===== Select an option ====");
                System.out.println("1. Say Hello");
                System.out.println("2. Get user data without project");
                System.out.println("3. Get user data with project");
                System.out.println("4. Get user list");
                System.out.println("0. Exit");
                System.out.println("===========================");
                BufferedReader teclat = new BufferedReader(new InputStreamReader(System.in));
                
                try{
                switch(Integer.parseInt(teclat.readLine())){
                    case 1:
                        sayHello(port);
                        break;
                    case 2:
                        getUserDataNoProject(port);
                        break;
                    case 3:
                        getUserDataWithProject(port);
                        break;
                    case 4:
                        getUserList(port);
                        break;
                    case 0:
                        System.out.println("Bye!");
                        return;
                }
                }catch (NumberFormatException ex) {                        
                }
            }while(menuOption != 0);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
   
    private static void sayHello(Prova port) {
        System.out.println("Server says: " + port.sayHello());
    }

    private static void getUserDataNoProject(Prova port) {
        System.out.print("Invoking getUserWithoutProject() on the server...");
        User u1 = port.getUserWithoutProject();
        System.out.println("OK");
        //System.out.println(u1.toString());
        System.out.println("USUARI:");
        System.out.println("id "+ u1.getId());
        System.out.println("username "+ u1.getUsername());
        System.out.println("nom "+ u1.getName());
        System.out.println();
    }

    private static void getUserDataWithProject(Prova port) {
        System.out.print("Invoking getUserWithProject() on the server...");
        User u1 = port.getUserWithProject();
        System.out.println("OK");
        //System.out.println(u1.toString());
        System.out.println("USUARI:");
        System.out.println("id "+ u1.getId());
        System.out.println("username "+ u1.getUsername());
        System.out.println("nom "+ u1.getName());
        System.out.println("projecte "+ u1.getProject().getName());
        System.out.println();
    }

    private static void getUserList(Prova port) {
        System.out.print("Invoking getListOfUsers() on the server...");
        System.out.println("OK");
        List<User> llista = port.getListOfUsers();
        System.out.println("Llista USUARIS:");
        for (User user : llista) {
            //System.out.println(user.toString());
            System.out.println("id "+ user.getId());
            System.out.println("username "+ user.getUsername());
            System.out.println("nom "+ user.getName());
            System.out.println();
        }
        
    }
    
}
