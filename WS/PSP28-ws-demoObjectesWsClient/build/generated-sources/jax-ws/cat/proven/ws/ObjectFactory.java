
package cat.proven.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cat.proven.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SayHello_QNAME = new QName("http://ws.proven.cat/", "sayHello");
    private final static QName _GetListOfUsersResponse_QNAME = new QName("http://ws.proven.cat/", "getListOfUsersResponse");
    private final static QName _GetUserWithProject_QNAME = new QName("http://ws.proven.cat/", "getUserWithProject");
    private final static QName _GetUserWithoutProjectResponse_QNAME = new QName("http://ws.proven.cat/", "getUserWithoutProjectResponse");
    private final static QName _GetUserWithProjectResponse_QNAME = new QName("http://ws.proven.cat/", "getUserWithProjectResponse");
    private final static QName _SayHelloResponse_QNAME = new QName("http://ws.proven.cat/", "sayHelloResponse");
    private final static QName _GetListOfUsers_QNAME = new QName("http://ws.proven.cat/", "getListOfUsers");
    private final static QName _Project_QNAME = new QName("http://ws.proven.cat/", "project");
    private final static QName _GetUserWithoutProject_QNAME = new QName("http://ws.proven.cat/", "getUserWithoutProject");
    private final static QName _User_QNAME = new QName("http://ws.proven.cat/", "user");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cat.proven.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetListOfUsersResponse }
     * 
     */
    public GetListOfUsersResponse createGetListOfUsersResponse() {
        return new GetListOfUsersResponse();
    }

    /**
     * Create an instance of {@link SayHello }
     * 
     */
    public SayHello createSayHello() {
        return new SayHello();
    }

    /**
     * Create an instance of {@link GetUserWithoutProjectResponse }
     * 
     */
    public GetUserWithoutProjectResponse createGetUserWithoutProjectResponse() {
        return new GetUserWithoutProjectResponse();
    }

    /**
     * Create an instance of {@link GetUserWithProject }
     * 
     */
    public GetUserWithProject createGetUserWithProject() {
        return new GetUserWithProject();
    }

    /**
     * Create an instance of {@link GetListOfUsers }
     * 
     */
    public GetListOfUsers createGetListOfUsers() {
        return new GetListOfUsers();
    }

    /**
     * Create an instance of {@link Project }
     * 
     */
    public Project createProject() {
        return new Project();
    }

    /**
     * Create an instance of {@link GetUserWithProjectResponse }
     * 
     */
    public GetUserWithProjectResponse createGetUserWithProjectResponse() {
        return new GetUserWithProjectResponse();
    }

    /**
     * Create an instance of {@link SayHelloResponse }
     * 
     */
    public SayHelloResponse createSayHelloResponse() {
        return new SayHelloResponse();
    }

    /**
     * Create an instance of {@link GetUserWithoutProject }
     * 
     */
    public GetUserWithoutProject createGetUserWithoutProject() {
        return new GetUserWithoutProject();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SayHello }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.proven.cat/", name = "sayHello")
    public JAXBElement<SayHello> createSayHello(SayHello value) {
        return new JAXBElement<SayHello>(_SayHello_QNAME, SayHello.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetListOfUsersResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.proven.cat/", name = "getListOfUsersResponse")
    public JAXBElement<GetListOfUsersResponse> createGetListOfUsersResponse(GetListOfUsersResponse value) {
        return new JAXBElement<GetListOfUsersResponse>(_GetListOfUsersResponse_QNAME, GetListOfUsersResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserWithProject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.proven.cat/", name = "getUserWithProject")
    public JAXBElement<GetUserWithProject> createGetUserWithProject(GetUserWithProject value) {
        return new JAXBElement<GetUserWithProject>(_GetUserWithProject_QNAME, GetUserWithProject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserWithoutProjectResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.proven.cat/", name = "getUserWithoutProjectResponse")
    public JAXBElement<GetUserWithoutProjectResponse> createGetUserWithoutProjectResponse(GetUserWithoutProjectResponse value) {
        return new JAXBElement<GetUserWithoutProjectResponse>(_GetUserWithoutProjectResponse_QNAME, GetUserWithoutProjectResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserWithProjectResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.proven.cat/", name = "getUserWithProjectResponse")
    public JAXBElement<GetUserWithProjectResponse> createGetUserWithProjectResponse(GetUserWithProjectResponse value) {
        return new JAXBElement<GetUserWithProjectResponse>(_GetUserWithProjectResponse_QNAME, GetUserWithProjectResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SayHelloResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.proven.cat/", name = "sayHelloResponse")
    public JAXBElement<SayHelloResponse> createSayHelloResponse(SayHelloResponse value) {
        return new JAXBElement<SayHelloResponse>(_SayHelloResponse_QNAME, SayHelloResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetListOfUsers }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.proven.cat/", name = "getListOfUsers")
    public JAXBElement<GetListOfUsers> createGetListOfUsers(GetListOfUsers value) {
        return new JAXBElement<GetListOfUsers>(_GetListOfUsers_QNAME, GetListOfUsers.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Project }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.proven.cat/", name = "project")
    public JAXBElement<Project> createProject(Project value) {
        return new JAXBElement<Project>(_Project_QNAME, Project.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserWithoutProject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.proven.cat/", name = "getUserWithoutProject")
    public JAXBElement<GetUserWithoutProject> createGetUserWithoutProject(GetUserWithoutProject value) {
        return new JAXBElement<GetUserWithoutProject>(_GetUserWithoutProject_QNAME, GetUserWithoutProject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link User }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.proven.cat/", name = "user")
    public JAXBElement<User> createUser(User value) {
        return new JAXBElement<User>(_User_QNAME, User.class, null, value);
    }

}
