/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.proven.ws;

/**
 *
 * @author mercedes
 */
public class ClientWS {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String respuesta = hello("Vicente");
        System.out.println(respuesta);
    }

    private static String hello(java.lang.String name) {
        cat.proven.ws.Saludo_Service service = new cat.proven.ws.Saludo_Service();
        cat.proven.ws.Saludo port = service.getSaludoPort();
        return port.hello(name);
    }
    
}
