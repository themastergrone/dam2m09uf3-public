/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.proven.ws;

import org.oorsprong.websamples.TCurrency;

/**
 *
 * @author mercedes
 */
public class JavaApp2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
        JavaApp2 myApp = new JavaApp2();
        myApp.run();
    }

    public void run() {
        TCurrency tc = countryCurrency("ES");        
        System.out.println("countryCurrency " + tc.getSName());
    }
    
    private TCurrency countryCurrency(java.lang.String sCountryISOCode) {
        org.oorsprong.websamples.CountryInfoService service = new org.oorsprong.websamples.CountryInfoService();
        org.oorsprong.websamples.CountryInfoServiceSoapType port = service.getCountryInfoServiceSoap();
        return port.countryCurrency(sCountryISOCode);
    }
    
}
