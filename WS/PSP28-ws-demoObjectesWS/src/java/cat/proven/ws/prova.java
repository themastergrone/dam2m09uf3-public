/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.proven.ws;

import cat.proven.entities.Project;
import cat.proven.entities.User;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebService;

/**
 *
 * @author alumne
 */
@WebService
public class prova {
    
    public String sayHello(){
        return "Hello! Use this web service to test object marshalling";
    }
    
    public User getUserWithProject(){
        return new User(1, "oriolper@gmail.com", "Oriol Pérez", new Project(1, "Desenvolupament d'aplicacions multiplataforma"));
    }
    
    public User getUserWithoutProject(){
        return new User(1, "oriolper@gmail.com", "Oriol Pérez");
    }
    
    public List<User> getListOfUsers(){
        List<User> result = new ArrayList<User>();
        
        Project p1 = new Project(1, "Projecte de DAM");
        Project p2 = new Project(2, "Projecte de DAW");
        Project p3 = new Project(3, "Projecte d'ASIX");
        
        result.add(new User(1, "user1@gmail.com", "User 1", p1));
        result.add(new User(2, "user2@gmail.com", "User 2"));
        result.add(new User(3, "user3@gmail.com", "User 3"));
        result.add(new User(4, "user4@gmail.com", "User 4", p3));
        result.add(new User(5, "user4@gmail.com", "User 5", p2));
        result.add(new User(6, "user4@gmail.com", "User 6", p1));
        return result;
    }
    
    
}
