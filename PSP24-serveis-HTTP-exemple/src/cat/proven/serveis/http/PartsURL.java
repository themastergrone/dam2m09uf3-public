/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.proven.serveis.http;

import java.net.MalformedURLException;
import java.net.URL;

/**
 *  Mostra els mètodes per obtenir les parts d'una URL
 * 
 *  https://docs.oracle.com/javase/8/docs/api/java/net/URL.html
 * 
 * @author mercedes
 */
public class PartsURL {

    /**
     * Definim una URL completa.
     * I imprimim cada part
     * 
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            // EXEMPLE class URL
            
            URL aURL = new URL("http://example.com:80/docs/books/tutorial"
                    + "/index.html?name=networking#DOWNLOADING");
            
            //Mostrem cada part
            System.out.println("protocol = " + aURL.getProtocol());
            System.out.println("authority = " + aURL.getAuthority());
            System.out.println("host = " + aURL.getHost());
            System.out.println("port = " + aURL.getPort());
            System.out.println("path = " + aURL.getPath());
            System.out.println("query = " + aURL.getQuery());
            System.out.println("filename = " + aURL.getFile());
            System.out.println("ref = " + aURL.getRef());
            
            //L'execucio mostrara:
            /*
                protocol = http
                authority = example.com:80
                host = example.com
                port = 80
                path = /docs/books/tutorial/index.html
                query = name=networking
                filename = /docs/books/tutorial/index.html?name=networking
                ref = DOWNLOADING
            
            */
            
        } catch (MalformedURLException ex) {
            System.out.println("Error " + ex);
        }
    }
    
}
