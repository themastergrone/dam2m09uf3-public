package cat.proven.serveis.http;

import java.net.*;
import java.io.*;
/**
 * Llegir un recurs per http, instanciant la URL i llegint del inputStream de la URL
 * 
 * @author mercedes
 */
public class DirectURLReader {
    public static void main(String[] args) throws Exception {
        //definim URL per una pagina amb http 
        URL pageURL = new URL("http://www.redirisnova.es/");
        
        //podem utilitzar directament el mètode openStream() de la class URL
        // aquest mètode obre la connexió i retorna el input stream
        BufferedReader in = new BufferedReader(new InputStreamReader(pageURL.openStream()));

        String inputLine;
        while ((inputLine = in.readLine()) != null)
            System.out.println(inputLine);
        in.close();
    }
}
    

