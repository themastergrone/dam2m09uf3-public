package cat.proven.serveis.http;

import java.net.*;
import java.io.*;
import java.util.Map;
import java.util.List;
/**
 * Llegir un recurs per http:
 * - instanciant la URL
 * - obrint la connexió
 * - llegir del inputStream de la URL
 * 
 * https://docs.oracle.com/javase/8/docs/api/java/net/URL.html
 * https://docs.oracle.com/javase/8/docs/api/java/net/HttpURLConnection.html
 * 
 * @author mercedes
 */
public class ClienteHTTP {
    public static void main(String[] args) throws Exception {
        //definim URL per una pagina amb http 
        URL pageURL = new URL("http://www.redirisnova.es/");
        
        //amb aquesta obtindrem HTTP code 301 (redirect)
        // ens redirigeix a https...
        //URL pageURL = new URL("http://www.proven.cat/");
        
        //obtenir un objecte URLConnection o subclass (HttpURLConnection)
        // a URLConnection podem modificar els paràmetres abans de fer la connexió
        
        //URLConnection connexio = pageURL.openConnection();
        HttpURLConnection connexioHTTP = (HttpURLConnection) pageURL.openConnection();
        //podriem afegir informació a les capçaleres
        connexioHTTP.setRequestMethod("GET");
        
        //connectem. En realitat no és necessari
        // els mètodes getInputStream, getOutputStream fan la connexio implicitament
        connexioHTTP.connect();
        
        //obtenim el HTTP CODE de la resposta
        System.out.println("RESPONSE CODE: " + connexioHTTP.getResponseCode() );
        
        //obtenir el inputStream
        BufferedReader in = new BufferedReader(new InputStreamReader(connexioHTTP.getInputStream()));
        
        //llegir linia a linia i mostrar per pantalla
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            System.out.println(inputLine);
        }
        in.close();
    }
}
    

